# This script simulates decaying of an initially flowing mass over slope as a result of decreasing the slope angle gradually (using a linear relation from an initial slope to a second value)

from yade import pack,ymport,export,geom,bodiesHandling, plot
import math
#from yade import qt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from numpy import arange

YELLOW = '\033[93m'
GREEN = '\033[92m'
BLUE = '\033[94m'
BLACK = '\033[0m'

#utils.readParamsFromTable(viscosityF=10)
#from yade.params.table import*
sp=pack.SpherePack()

O.periodic=True

#params of the depositon phase
compFricDegree=10.
finalFricDegree=10.
num_spheres=-1

#sphere material and geom params
p_finalFricDegree=finalFricDegree
p_diam=0.02
p_EY=1e5
p_density=400
p_rho = 400
viscosityF_Deposition=2
viscosityF=2
#estimated height of the sphere pack
height=p_diam*50.
#estimated length of the sphere pack
length=p_diam*8
width=length
theta=radians(25.)
theta_ini=radians(0.)
#dimensions of the cell
O.cell.hSize=Matrix3(   1*width, 0, 0,
                        0 ,3*height, 0,
                        0, 0, 1*length);


#flowEngine parameters

 
### lubrication model parameters
l_epsilon=0.02# Roughness for lubrication model: fraction of radius enlargement for contact [=0.001]
#l_Eta=viscosityF

#####
g=9.81 #gravity acc
#theta=radians(24.) #channel inclination


#add boxes as first 2 bodies
O.materials.append(FrictMat(density=p_rho,young=p_EY,poisson=0.3,frictionAngle=radians(p_finalFricDegree),label='boxMat'))
#upBox = utils.box( center=(0,1.8*height,0),orientation=Quaternion(1,0,0,0), extents=(length*100.0,0,width*100.0) ,fixed=1,wire=False,material='boxMat')
lowBox = utils.box( center=(length*0.5,0,width*0.5),orientation=Quaternion(1,0,0,0), extents=(length*2,0,width*2) ,fixed=1,wire=False, material='boxMat')
O.bodies.append([lowBox])

#make the upbox invisible to the particles during the deposition
#upBox.mask=2

lawFrictLub = Law2_ScGeom_ImplicitLubricationPhys( activateTangencialLubrication=1,
                                                activateTwistLubrication=0,
                                                activateRollLubrication=0,
                                                  resolution=0,
                                                label='lublaw')


#enlarged factor for the contact detection
enlFactor=2.
####################################################
O.engines=[
    ForceResetter(),
    InsertionSortCollider([Bo1_Box_Aabb(),Bo1_Sphere_Aabb(aabbEnlargeFactor=enlFactor)],verletDist=-0.1,allowBiggerThanPeriod=True),
    InteractionLoop(
        [Ig2_Sphere_Sphere_ScGeom6D(interactionDetectionFactor=enlFactor),Ig2_Box_Sphere_ScGeom6D(interactionDetectionFactor=enlFactor)],
        [Ip2_FrictMat_FrictMat_LubricationPhys(eta=viscosityF_Deposition,eps=l_epsilon,keps=5,label='ip2_lub')],
        [lawFrictLub]
    ),
    GlobalStiffnessTimeStepper(active=1,timeStepUpdateInterval=100,timestepSafetyCoefficient=0.8, defaultDt=0.6*utils.PWaveTimeStep(),label="ts"),
   # PeriTriaxController(label='triax', goal=(sigmaIso, sigmaIso, sigmaIso), stressMask=7,dynCell=True,maxStrainRate=(10, 10, 10),maxUnbalanced=.1,relStressTol=1e-3,doneHook='compactionFinished()'),
    NewtonIntegrator(damping=0.,gravity=(0.,-g,0.),label='newton'),
    PyRunner(initRun=0,command='addData()',iterPeriod=1003,label='adddata'),
        
]


newton.gravity=(sin(theta_ini)*g,cos(theta_ini)*(-g),0)
#print ('gravity is ',newton.gravity)

               
                
def checkStab():
    if unbalancedForce()<=0.02:
        print ('sample is stable')
        print ('unbF=',unbalancedForce())
        O.pause()
        setContactFriction(radians(finalFricDegree))
        #eraseInt()
        O.step()
        glueSpheres(spheres)
        export.text('VerticalDepSample_E5e6.txt')
        O.save('VerticalDepSample_E5e6.yade.gz')
    else:
        print ('unbF=',unbalancedForce())

def eraseInt():
    for i in O.interactions:
        O.interactions.erase(i.id1,i.id2)

##############################################################################
#                           DEPOSITION
##############################################################################
flowPhase=False
##import sphere pack
O.materials.append(FrictMat(density=p_density,young=p_EY,poisson=0.3,frictionAngle=radians(compFricDegree),label='sphereMat'))

##################
# first phase: vertical deposition
if os.path.exists('VerticalDepSample_E5e6.yade.gz'):
    flowPhase=True
    print ('sphere pack exists already')
    O.load('VerticalDepSample_E5e6.yade.gz')
    #eraseInt() # otherwise the friction value still be the one of the depositon (probably history dep of lubLaw)
    # second phase: inclined stabilization
    checkstab.dead=1  #modificare
    spheres=[]
    for b in O.bodies:
        if b.id>1:
            spheres += [b]

else:
#    #import initial sphere pack --> to have the same for all depositions
    if os.path.exists('initialPacking.spheres'):
        O.bodies.append(ymport.text('initialPacking.spheres',material='sphereMat'))
    else:
        sp.makeCloud((0,0.5*height,0),(length,2.0*height+1*p_diam,width),p_diam/2.,0.05,num_spheres,periodic=0,seed=1)
        O.bodies.append([utils.sphere(s[0],s[1],material='sphereMat') for s in sp])
        export.text('initialPacking.spheres')
    #sample stabilization
    O.step()
    print ('unbF=',unbalancedForce())
    
   # ip2_lub.eta=0   #Deposition with zero viscosity


    O.engines= O.engines+[PyRunner(command='checkStab()',iterPeriod=2500,label='checkstab')]
    spheres = []
    for b in O.bodies:
        if b.id>1:
            spheres += [b]
    for i in spheres:
        i.shape.color=[0.2,.8,.0]

###########################################

##compute some quantities
listRad=[]
x_pos_time0=[]
for b in O.bodies:
  if (b.id>1):
    listRad += [b.shape.radius]
    x_pos_time0 +=[b.state.pos[0]]
meanRad = sum(listRad) / len(listRad)
maxRad = max(listRad)

xmax=([(spheres[i].state.pos[0]) for i in range(len(spheres))])
Xmax=max(xmax)
Xmin=min(xmax)
ymax=([(spheres[i].state.pos[1]) for i in range(len(spheres))])
Ymax=max(ymax)
Ymin=min(ymax)
zmax=([(spheres[i].state.pos[2]) for i in range(len(spheres))])
Zmax=max(zmax)
Zmin=min(zmax)

X_center=(Xmax-Xmin)/2.

#for computing solid fraction along y profile
#i define slices along y direction
cutBorder_y2=3.*(2.*meanRad)

xmin=0+2*meanRad
xmax=O.cell.hSize[0][0]-2*meanRad

ymin=Ymin
ymax=Ymax+3*(2*meanRad)

zmin=0+2*meanRad
zmax=O.cell.hSize[2][2]-2*meanRad

ymin2=Ymin+cutBorder_y2
ymax2=Ymax-cutBorder_y2
delta = 2.*meanRad #thickness of REV slice
height=(ymax-ymin)
height2=(ymax2-ymin2)
print("Height of flow:", Ymax-Ymin)
ny=math.trunc(height/delta)
ny2=math.trunc(height2/delta)

TW=TesselationWrapper()
#TW.triangulate()        #compute regular Delaunay triangulation, don’t construct tesselation
#TW.computeVolumes()     #will silently tesselate the packing, then compute volume of each Voronoi cell
#TW.volume(10)           #get volume associated to sphere of id 10
TW.setState(0)          #store current positions internaly for later use as the "0" state

O.step()        #make particles move a little (let's hope they will!)
TW.setState(1)          #store current positions internaly in the "1" (deformed) state
#Now we can define strain by comparing states 0 and 1, and average them at the particles scale
TW.defToVtk("strain.vtk")
#if not os.path.isfile("strain.vtk") : raise YadeCheckError("no output file")
#os.remove("strain.vtk")

s=bodyStressTensors()
##################################################
#########     SOME FUNCTIONS
##################################################   
def velProfile(lista,axis,ymin,ymax,ny,l):
    list_y=np.zeros(ny+1)    
    list2=np.zeros(ny+1)  
    list_vel_x=np.zeros(ny+1)
    list_vel_y=np.zeros(ny+1)
    list_vel_z=np.zeros(ny+1)
    List_Str_Rat=np.zeros(ny+1)
    List_I_V=np.zeros(ny+1)
    List_Re=np.zeros(ny+1)

    #List_I_V2=np.zeros(ny+1)

    for j in range(0,ny+1):
        weights=0; vel_x=0; vel_y=0; vel_z=0;
        y=ymin+j*(ymax-ymin)/float(ny)
        for b in lista:
            r= ((y-b.state.pos[axis])**2)**0.5  
            w=weight(r,l)
            weights +=w
            vel_x +=w*b.state.vel[0]
            vel_y +=w*b.state.vel[1]
            vel_z +=w*b.state.vel[2]
        list_y[j]=y
        list_vel_x[j]=vel_x/weights
        list_vel_y[j]=vel_y/weights
        list_vel_z[j]=vel_z/weights
        List_Str_Rat[j]=(list_vel_x[j]-list_vel_x[j-1])/delta
        List_I_V[j]=viscosityF*List_Str_Rat[j]/(p_rho*g*  (ymax-list_y[j]+0.000001)*  cos(theta))
        #List_I_V2[j]=viscosityF*List_Str_Rat[j]/(getStress()[1])
    #Q=(np.vstack((list_y,list_vel_x,list_vel_y,list_vel_z)))
        #stress = s[b.id]*4.*pi/3.*b.shape.radius**3/TW.volume(b.id)
        #List_Re[j]=p_rho*list_vel_x[j]*p_diam/(stress[1][1]/O.cell.getSmallStrain()[1][1])

    Q_x=numpy.append(np.array([O.time]),list_vel_x)
    if not os.path.exists("velXProfile_axis"+str(axis)):
        np.savetxt("velXProfile_axis"+str(axis),list([np.zeros(len(Q_x))]),header='vel_x  ----  axis='+str(axis)+str(list([list_y])))
    else:
        with open("velXProfile_axis"+str(axis), "ab") as f:
            #f.write(b"\n")       
            #np.savetxt(f, Q)
            np.savetxt(f,list([Q_x]))

    Q_y=numpy.append(np.array([O.time]),list_vel_y)
    if not os.path.exists("velYProfile_axis"+str(axis)):
        np.savetxt("velYProfile_axis"+str(axis),list([np.zeros(len(Q_y))]),header='vel_y  ----  axis='+str(axis)+str(list([list_y])))
    else:
        with open("velYProfile_axis"+str(axis), "ab") as f:
            #f.write(b"\n")       
            #np.savetxt(f, Q)
            np.savetxt(f,list([Q_y]))
                
    Q_z=numpy.append(np.array([O.time]),list_vel_z)
    if not os.path.exists("velZProfile_axis"+str(axis)):
        np.savetxt("velZProfile_axis"+str(axis),list([np.zeros(len(Q_z))]),header='vel_z  ----  axis='+str(axis)+str(list([list_y])))
    else:
        with open("velZProfile_axis"+str(axis), "ab") as f:
            #f.write(b"\n")       
            #np.savetxt(f, Q)
            np.savetxt(f,list([Q_z]))


    # Q_SR=numpy.append(np.array([O.time]),List_Str_Rat)
    # if not os.path.exists("Strain_Rate"):
    #     np.savetxt("Strain_Rate",list([np.zeros(len(Q_SR))]),header='Strain_Rate  ----  axis='+str(1)+str(list([list_y])))
    # else:
    #     with open("Strain_Rate", "ab") as f:
    #            np.savetxt(f,list([Q_SR]))

    Q_Iv=numpy.append(np.array([O.time]), List_I_V)
    if not os.path.exists("Viscous_Num"):
        np.savetxt("Viscous_Num",list([np.zeros(len(Q_Iv))]),header='Viscous_Num  ----  axis='+str(1)+str(list([list_y])))
    else:
        with open("Viscous_Num", "ab") as f:
               np.savetxt(f,list([Q_Iv]))

    #Q_Re=numpy.append(np.array([O.time]),List_Re)
    # if not os.path.exists("Re_number"+str(axis)):
    #     np.savetxt("Re_number"+str(axis),list([np.zeros(len(Q_Re))]),header='Re  ----  axis='+str(axis)+str(list([List_Re])))
    # else:
    #     with open("Re_number"+str(axis), "ab") as f:
    #         #f.write(b"\n")       
    #         #np.savetxt(f, Q)
    #         np.savetxt(f,list([Q_Re]))
    #return [list_y,list_velx]
###############################################

def computeSolFract_new(lista,axis,Ymin,Ymax,length):
  
    ny=50
    Deltay=(Ymax-Ymin)/ny
    list_cell=np.zeros(ny)
    list_SolFract=np.zeros(ny)
    for j in range (1,ny):
     y_low_bound=(j-1)*Deltay
     y_up_bound=j*Deltay
     volumeCell=Deltay*length*length;volumeSolid=0
     for b in lista:
          if (b.state.pos[axis]>y_low_bound and b.state.pos[axis]<y_up_bound):
            volumeSolid+=4.*pi/3.*(b.shape.radius*(1+ip2_lub.eps))**3
            
     list_SolFract[j]=volumeSolid/volumeCell
     list_cell[j]=j
    SF_y=numpy.append(np.array([O.time]),list_SolFract)
    if not os.path.exists("Solid_Frac_new"):
        np.savetxt("Solid_Frac_new",list([np.zeros(len(SF_y))]),header='Solid_Frac  ----  axis='+str(1)+str(list([list_cell])))
    else:
        with open("Solid_Frac_new", "ab") as f:
               np.savetxt(f,list([SF_y]))
    return [list_cell,list_SolFract,np.mean(list_SolFract)]




def computeSolFract(lista,axis,ymin2,ymax2,ny2,l2):
    list_y=np.zeros(ny2+1)
    list_SolFract=np.zeros(ny2+1)
    #TW=PeriodicFlowEngine(duplicateThreshold=meanRad*6.)#,wallIds=[-1,-1,upBox.id,lowBox.id,-1,-1])
    TW=TesselationWrapper()
    TW.computeVolumes() 
    for j in range(0,ny2+1):
            y=ymin2+j*(ymax2-ymin2)/float(ny2)
            weights=0; volumeTotal=0;volumeSolid=0
            for b in lista:
                #if type(b.shape)==Sphere:
                    r= ((y-b.state.pos[axis])**2)**0.5 
                    w=weight(r,l2)
##                    to check if slices are taken correctly
#                    if j%2 ==0 and b.state.pos[1]>y-(ymax-ymin)/float(ny):
#                            b.shape.color=[.1,.1,min(1.,1.*w)]            
#                    elif j%2 !=0 and b.state.pos[1]>y-(ymax-ymin)/float(ny):
#                            b.shape.color=[.1,min(1.,1.*w),.1]            
                    weights+=w
                    volumeSphere=4.*pi/3.*(b.shape.radius*(1+ip2_lub.eps))**3
                    volumeCell=TW.volume(b.id)
                    #print(volumeCell)
                    volumeTotal+=w*volumeCell
                    volumeSolid+=w*volumeSphere
                    b.spherePoro=1.-volumeSphere/volumeCell
            localSolFract=volumeSolid/(volumeTotal)   # to avoid division by zero
            list_y[j]=y
            list_SolFract[j]=localSolFract  
    SF_y=numpy.append(np.array([O.time]),list_SolFract)
    if not os.path.exists("Solid_Frac"):
        np.savetxt("Solid_Frac",list([np.zeros(len(SF_y))]),header='Solid_Frac  ----  axis='+str(1)+str(list([list_y])))
    else:
        with open("Solid_Frac", "ab") as f:
               np.savetxt(f,list([SF_y]))
    return [list_y,list_SolFract,np.mean(list_SolFract)]
    

def profile():
    velProfile(spheres,1,ymin,ymax,ny,delta)

def SolidFrac():
    computeSolFract(spheres,1,ymin2,ymax2,ny2,3*p_diam)
    
    
def SolidFrac_new():
    computeSolFract_new(spheres,1,Ymin,Ymax,length)
###############################################
def work_Eg(): #work of the grav energy
    j=0
    work_eg=0.
    for b in O.bodies:
        if (b.id>1):
            work_eg +=(b.state.pos[0]-x_pos_time0[j])*newton.gravity[0]*b.state.mass
            j=j+1
    return work_eg

    
###############################################
def massflowrate(lista):
    FR = 0
    N=0
    V=0
    Section_area=height*width
    for b in lista:
        #if(b.state.pos[0]>=(length/2-p_diam/2) and b.state.pos[0]<=(length/2+p_diam/2)):
         b.shape.color=[0,0,1]
         N =N+1
         V +=b.state.vel[0]
        #  S=getStress()
        #  print(S)
    #FR += p_density * Section_area *b.state.vel[0]
    #print(N)
    FR += p_density * Section_area *V/N
    return FR

###############################################
# def Iv(lista): #Viscous number
#     Iv = 0
    
#     for b in lista:
#         if(b.state.pos[0]>=(length/2-p_diam) and b.state.pos[0]<=(length/2+p_diam)):
#          Iv = viscosityF * Strain_rate/Pressure
#     return Iv    

###############################################
def cm_vel(list_bodies): #velocity of the center of mass
    mTOT=0.    
    mvel=Vector3(0.,0.,0.)
    for b in list_bodies:
        if b.state.blockedDOFs!='xyzXYZ':
            mvel +=b.state.mass*b.state.vel
            mTOT +=b.state.mass
    vel_cm=mvel/mTOT
    return vel_cm


def cm_loc(list_bodies): #location of the center of mass
    mTOT=0.    
    mloc=Vector3(0.,0.,0.)
    for b in list_bodies:
        if b.state.blockedDOFs!='xyzXYZ':
            mloc +=b.state.mass*b.state.pos
            mTOT +=b.state.mass
    loc_cm=mloc/mTOT
    return loc_cm



###############################################
def weight(r,l):
  return exp(-(abs(r/l)**2))
###############################################
def velTop(lista,axis,y_ref):
    delta=3*p_diam
    weights=0; vel_x=0;
    y=y_ref
    for b in lista:
        if b.state.blockedDOFs!='xyzXYZ':
            r= ((y-b.state.pos[1])**2)**0.5  
            w=weight(r,delta)
            weights +=w
            vel_x +=w*b.state.vel[axis]
    vel_ref=vel_x/weights
    return vel_ref
###############################################
def RotateGrav():
    if round(degrees(np.arcsin(abs(newton.gravity[0]/g))),1)<round(degrees(np.arccos(abs(newton.gravity[1]/g))),1):
        plus_theta=round(degrees(np.arcsin(abs(newton.gravity[0]/g))),1)+.1
        print ('new_theta is=',plus_theta)
        newton.gravity[0]=sin(radians(plus_theta))*g
    else:
        print('rotation is done')
###############################################
def kin_en(list_bodies): #velocity of the center of mass
    kin_en=Vector3(0.,0.,0.)
    kin_enTOT=0
    for b in list_bodies:
        kin_en +=.5*b.state.mass*np.power(b.state.vel,2)
    return kin_en
###############################################
if flowPhase:
    ##############################################################################
    #                           FLOW PHASE                                       #
    ##############################################################################
    adddata.dead=1  #dead adddata
    #set time to zero after the tilting of the box
    O.resetTime()
    
    newton.gravity=(sin(theta)*g,cos(theta)*(-g),0)
    
    
    O.engines = O.engines+[PyRunner(initRun=1,command='Rotateslope()',iterPeriod=500,label='Rotateslope')]

    print('gravity is rotated by ',degrees(theta))    
    print('start flowPhase')
    ip2_lub.eta=viscosityF
    
    ##################################################
    
    
    def Rotateslope():
      
      
      T=100
      T_final_state=200
      if(O.time>T):   # Add a statement in order to not modeling the negative values of slope angles
        Theta_ini=radians(25.)
        Theta_F=radians(15.)
        T_ini=T
        T_F=200
        Theta_inst=((Theta_F-Theta_ini)/(T_F-T_ini))*O.time+(Theta_ini*T_F-Theta_F*T_ini)/(T_F-T_ini) # Should be modifed in order to not modeling the negative values of slope angles
        print("Gradually decreasing of slope is started at",O.time)
        print("Gradually decreasing of slope is started at",O.time)
        newton.gravity=(sin(Theta_inst)*g,cos(Theta_inst)*(-g),0.)

        
    
    
    
    
    def addData2():
        ####################################################
        plot.addData(
            unb=unbalancedForce(),
            dt=O.dt,
            time=O.time,
            e_k=utils.kineticEnergy(),
            e_kx=kin_en(spheres)[0],
            e_ky=kin_en(spheres)[1],
            w_eg=work_Eg(),
            m_fl_rate=massflowrate(spheres),
            # sxx=triax.stress[0],
            # syy=triax.stress[1],
            # szz=triax.stress[2],
            # exx=triax.strain[0],
            # eyy=triax.strain[1],
            # ezz=triax.strain[2],
            cmvel=cm_vel(spheres)[0],
            cmloc=cm_loc(spheres)[1],
            topvel=velTop(spheres,0,aabbExtrema()[1][1]-3*p_diam),
            #p=flow.averageSlicePressure(Ymin+meanRad),  #at the base of the sample
            H_y=aabbExtrema()[1][1]-aabbExtrema()[0][1],
            )
    def saveData():
        plot.saveDataTxt('delayedAvalanche_theta'+str(round(np.rad2deg(theta),1))+'.txt')
    plot.plots={' time':('cmvel','topvel'),'time':'m_fl_rate','time':'cmloc'}
    O.engines = O.engines+[PyRunner(initRun=1,command='addData2()',iterPeriod=500,label='adddata2')]
    #to export vtk files
    expVTK=1
    if expVTK:
        if not os.path.exists('VTKspheres') :
            os.makedirs('VTKspheres')
        vtk_virtStep=0.025
       
        O.engines = O.engines+[VTKRecorder(initRun=1,fileName='VTKspheres/',virtPeriod=vtk_virtStep,
                                           recorders=['spheres','velocity','force','colors'],dead=0,label='vtk')]
    expData=1
    if expData:
        O.engines = O.engines+[PyRunner(initRun=1,command='saveData()',iterPeriod=1000)]
        O.engines = O.engines+[PyRunner(initRun=1,command='profile()',virtPeriod=0.025,label='profdata')]
        O.engines=O.engines+[PyRunner(initRun=1,command='SolidFrac()',virtPeriod=0.025,label='SolidFrac' )]
        O.engines=O.engines+[PyRunner(initRun=1,command='SolidFrac_new()',virtPeriod=0.025,label='SolidFrac_new' )]
    gluedSpheres=[]      
    for b in spheres:
        if b.state.blockedDOFs=='xyzXYZ':
            spheres.remove(b)
            gluedSpheres +=[b]
    ts.active=0  #disable global time stepper
    print ('chehck dt')
    dt_stiff=PWaveTimeStep()            
    
else:
    def addData():
        ####################################################
        plot.addData(
            unb=unbalancedForce(),
            time=O.time,
            e_k=utils.kineticEnergy(),
            e_kx=kin_en(spheres)[0],
            e_ky=kin_en(spheres)[1],
            cmvel=cm_vel(spheres)[0],
            )
    plot.plots={'time':('unb'),' time':('cmvel'),' time ':('e_kx','e_ky','e_k')}
    #to save some data



          
##################################################
#########     OTHER FUNCTIONS
##################################################            
            
def savePlot():
    if os.path.exists('someFile.pdf'):
        os.remove('someFile.pdf')
    plot.plot(noShow=True).savefig('someFile.pdf')


def checkTop():
    for i in spheres:
        if i.state.pos[1]>=aabbExtrema()[1][1]-3*p_diam:
            i.shape.color=[0,0,1]

    
#fix a bottom layer of particles
def glueSpheres(lista):
    for s in lista:
        if s.state.pos[1]<=lowBox.state.pos[1]+p_diam/2.*3.:       
                    s.state.blockedDOFs='xyzXYZ'
                    s.shape.color=[1,0,0]
                    s.state.vel=(0,0,0)
                    O.forces.setPermF(s.id,Vector3(0.,0.,0.))
    #return gluedSpheres
##################################################     

plot.plot(subPlots=True)
